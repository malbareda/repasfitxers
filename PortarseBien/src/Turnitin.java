import java.io.Serializable;

public class Turnitin implements Serializable {
	
	double porcentajecopia;
	String partido="PSOE";
	String[] padrinos = new String[2];
	int paginastesis;
	String personaALaQuePlagiaste;
	
	public Turnitin(double pc, String padrino1, String padrino2, int paginas, String plagiado) {
		
		porcentajecopia=pc;
		padrinos[0]=padrino1;
		padrinos[1]=padrino2;
		paginastesis = paginas;
		this.personaALaQuePlagiaste=plagiado;		
	}
	
	public String toString() {
		return "La Tesis de PDR, tiene "+paginastesis+" paginas y le ha plagiado a "+this.personaALaQuePlagiaste+" un "+this.porcentajecopia+" % de la tesis";
	}

}

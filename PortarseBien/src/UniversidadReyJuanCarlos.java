import java.io.Serializable;

public class UniversidadReyJuanCarlos implements Serializable{

	
	int exigencia = 0;
	int precio;
	int grados;
	int masters;
	int mastersPeroDeVerdad = 0;
	String[] tribunal = new String[3];
	Rector rector = new Rector();
	
	
	
	public UniversidadReyJuanCarlos(int precio, int grados, int masters, String Tribunal1, String Tribunal2, String Tribunal3, String rector) {
		
		this.precio=precio;
		this.grados=grados;
		this.masters=masters;
		this.tribunal[0]=Tribunal1;
		this.tribunal[1]=Tribunal2;
		this.tribunal[2]=Tribunal3;
		this.rector.nombre=rector;
		this.rector.anosDeCarcel=20;

	}
	
	public String toString() {
		return "Universidad Rey Juan Carlos, el rector es "+rector.nombre+" tenemos" +grados+ "grados y tu tribunal se compone de "+tribunal[0]+" "+tribunal[1]+" "+tribunal[2]+" ";
		
	}
	
	
}
